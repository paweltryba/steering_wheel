//���������������������������������������������������������������������������
//�                                                                         �
//� Module: Internals Example Header File                                   �
//�                                                                         �
//� Description: Declarations for the Internals Example Plugin              �
//�                                                                         �
//�                                                                         �
//� This source code module, and all information, data, and algorithms      �
//� associated with it, are part of CUBE technology (tm).                   �
//�                 PROPRIETARY AND CONFIDENTIAL                            �
//� Copyright (c) 1996-2014 Image Space Incorporated.  All rights reserved. �
//�                                                                         �
//�                                                                         �
//� Change history:                                                         �
//�   tag.2005.11.30: created                                               �
//�                                                                         �
//���������������������������������������������������������������������������

#ifndef _INTERNALS_EXAMPLE_H
#define _INTERNALS_EXAMPLE_H

#include "InternalsPlugin.hpp"
#include <Windows.h>
#include <math.h>

// This is used for the app to use the plugin for its intended purpose
class ExampleInternalsPlugin : public InternalsPluginV01  // REMINDER: exported function GetPluginVersion() should return 1 if you are deriving from this InternalsPluginV01, 2 for InternalsPluginV02, etc.
{

 public:

  // Constructor/destructor
  ExampleInternalsPlugin() 
  {
	  //m_ffbOutput = fopen("FFB.txt", "a");
  }
  ~ExampleInternalsPlugin() {}

  // These are the functions derived from base class InternalsPlugin
  // that can be implemented.
  void Startup( long version );  // game startup
  void Shutdown();               // game shutdown

  void EnterRealtime();          // entering realtime
  void ExitRealtime();           // exiting realtime

  void StartSession();           // session has started
  void EndSession();             // session has ended

  // GAME OUTPUT
  long WantsTelemetryUpdates() { return( 1 ); } // CHANGE TO 1 TO ENABLE TELEMETRY EXAMPLE!
  void UpdateTelemetry( const TelemInfoV01 &info );

  bool WantsGraphicsUpdates() { return( false ); } // CHANGE TO TRUE TO ENABLE GRAPHICS EXAMPLE!
  void UpdateGraphics( const GraphicsInfoV01 &info );

  // GAME INPUT
  bool HasHardwareInputs() { return( false ); } // CHANGE TO TRUE TO ENABLE HARDWARE EXAMPLE!
  void UpdateHardware( const double fDT ) { mET += fDT; } // update the hardware with the time between frames
  void EnableHardware() { mEnabled = true; }             // message from game to enable hardware
  void DisableHardware() { mEnabled = false; }           // message from game to disable hardware

  // See if the plugin wants to take over a hardware control.  If the plugin takes over the
  // control, this method returns true and sets the value of the double pointed to by the
  // second arg.  Otherwise, it returns false and leaves the double unmodified.
  bool CheckHWControl( const char * const controlName, double &fRetVal );

  bool ForceFeedback( double &forceValue );  // SEE FUNCTION BODY TO ENABLE FORCE EXAMPLE

  // SCORING OUTPUT
  bool WantsScoringUpdates() { return( false ); } // CHANGE TO TRUE TO ENABLE SCORING EXAMPLE!
  void UpdateScoring( const ScoringInfoV01 &info );

  // COMMENTARY INPUT
  bool RequestCommentary( CommentaryRequestInfoV01 &info );  // SEE FUNCTION BODY TO ENABLE COMMENTARY EXAMPLE

  // CUSTOM PAWEL DEVICE WHEEL
  void zapisz(const char* const msg);
  int Close_Comm(HANDLE hCommDev);
  int Wyslij(HANDLE hCommDev, LPCVOID lpBuffer, DWORD nNumberOfBytesToWrite);
  int Odbierz(HANDLE hCommDev, LPVOID lpBuffer, LPDWORD lpNumberOfBytesRead, DWORD Buf_Size);
  unsigned char toChar(int liczba);
  int toInt(unsigned char znak);
  void time2char(char tab[8], double time);
  double zaokraglij_do_1(double x);
  void UpdateWheelData(const TelemInfoV01& info);

  HANDLE  hCommDev;           // identyfikator portu
  DCB     dcb;                // struktura kontroli portu szeregowego
  DWORD Number_Bytes_Read;    // Number Bytes to Read � liczba bajt�w do czytania
  LPCTSTR lpFileName;         // wska�nik do nazwy portu
  DWORD fdwEvtMask;           // informacja o aktualnym stanie transmisji
  COMSTAT Stat;               // dodatkowa informacja o zasobach portu
  DWORD Errors;               // reprezentuje typ ewentualnego b��du
  bool czy_otwarty;
  long s_bieg;
  int pozycja;
  unsigned char flaga[3];
  char LED_lewy[8];
  char LED_prawy[8];
  double mBestLapTime; //najlepszy czas
  double mLastLapTime; //czas ostatniego okrazenia

 private:

  void WriteToAllExampleOutputFiles( const char * const openStr, const char * const msg );
  double mET;  // needed for the hardware example
  bool mEnabled; // needed for the hardware example
  //FILE* m_ffbOutput = nullptr;
};


#endif // _INTERNALS_EXAMPLE_H

