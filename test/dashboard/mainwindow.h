#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QTimer>
#include <QTime>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_portsRefresh_clicked();
    void on_pushButton_portOpen_clicked();
    void on_pushButton_portClose_clicked();
    void readyRead();
    void on_pushButton_clicked();
    void on_pushButton_showUsbDevice_clicked();
    void on_pushButton_2_clicked();
    void on_rpm0_toggled(bool );
    void on_rpm1_toggled(bool );
    void on_rpm2_toggled(bool );
    void on_rpm3_toggled(bool );
    void on_rpm4_toggled(bool );
    void on_rpm5_toggled(bool );
    void on_rpm9_toggled(bool );
    void on_rpm6_toggled(bool );
    void on_rpm7_toggled(bool );
    void on_rpm8_toggled(bool );
    void on_rpm10_toggled(bool );
    void on_rpm11_toggled(bool );
    void on_rpm12_toggled(bool );
    void on_rpm13_toggled(bool );
    void on_rpm14_toggled(bool );
    void on_flagBlueLeft_toggled(bool );
    void on_flagGreenLeft_toggled(bool );
    void on_flagRedLeft_toggled(bool );
    void on_flagYellowLeft_toggled(bool );
    void on_flagYellowRight_toggled(bool );
    void on_flagRedRight_toggled(bool );
    void on_flagGreenRight_toggled(bool );
    void on_flagBlueRight_toggled(bool);
    void on_gear_currentIndexChanged(int index);
    void on_wys0_currentIndexChanged(int index);
    void on_wys1_currentIndexChanged(int index);
    void on_wys2_currentIndexChanged(int index);
    void on_wys3_currentIndexChanged(int index);
    void on_wys4_currentIndexChanged(int index);
    void on_wys5_currentIndexChanged(int index);
    void on_wys6_currentIndexChanged(int index);
    void on_wys7_currentIndexChanged(int index);
    void on_horizontalSlider_rpm_sliderMoved(int position);
    void updateDashboard();
    void updateDisplay();

private:
    Ui::MainWindow *ui;
    QSerialPort *m_serialPortSensor;
    QTimer m_updateDashboard;
    QTimer m_updateDisplay;
    char getLeftRpm();
    char getRightRpm();
    char getFlags();
    void updateRpm(int p_rpm);


    int m_switchN14{};
    int m_encoderUpper{};
    int m_encoderBottom{};
    int m_rpm{};
    QTime m_option3Time{};
    char displayNumber[8] = {'N', 'O', 'K', 'I', 'A', '-', '-', '-'};
};

#endif // MAINWINDOW_H
