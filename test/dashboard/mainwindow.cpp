#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <algorithm>
#include <QDebug>
#include <QSerialPort>
#include <QSerialPortInfo>
#include "usbproxy.h"
#include <iostream>
#include <QTimer>
#include <bitset>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_updateDashboard(this)
{
    ui->setupUi(this);
    m_serialPortSensor = new QSerialPort(this);
    qDebug() << "Number of serial ports:" << QSerialPortInfo::availablePorts().count();
    ui->comboBox_portsName->clear();
    foreach (const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
    {
        qDebug() << "\nPort:" << serialPortInfo.portName();
        ui->comboBox_portsName->addItem(serialPortInfo.portName());
    }

    QString symbols16Seg = "0123456789ACEFGHIJKLMNOPRSTUWXYZbcdhnrtu+-/\\";
    for(QChar symbol : symbols16Seg)
    {
        ui->wys0->addItem(symbol, QVariant::String);
        ui->wys1->addItem(symbol, QVariant::String);
        ui->wys2->addItem(symbol, QVariant::String);
        ui->wys3->addItem(symbol, QVariant::String);
        ui->wys4->addItem(symbol, QVariant::String);
        ui->wys5->addItem(symbol, QVariant::String);
        ui->wys6->addItem(symbol, QVariant::String);
        ui->wys7->addItem(symbol, QVariant::String);
    }

    QString symbols7Seg = "0123456789ACEFHJLOPSU-nr";
    for(QChar symbol : symbols7Seg)
    {
        ui->gear->addItem(symbol, QVariant::String);
    }
    ui->gear->setCurrentIndex(5);

    ui->wys0->setCurrentIndex(1);
    ui->wys1->setCurrentIndex(2);
    ui->wys2->setCurrentIndex(3);
    ui->wys3->setCurrentIndex(4);
    ui->wys4->setCurrentIndex(6);
    ui->wys5->setCurrentIndex(7);
    ui->wys6->setCurrentIndex(8);
    ui->wys7->setCurrentIndex(9);


    QObject::connect(&m_updateDashboard, SIGNAL(timeout()), this, SLOT(updateDashboard()));
    m_updateDashboard.start(50);

    QObject::connect(&m_updateDisplay, SIGNAL(timeout()), this, SLOT(updateDisplay()));
    m_updateDisplay.start(1000);
}

MainWindow::~MainWindow()
{
    if (m_serialPortSensor->isOpen())
    {
        m_serialPortSensor->close();
        qDebug() << "...serial port is closed!";
    }
    delete ui;
}

void MainWindow::on_pushButton_portsRefresh_clicked()
{
    qDebug() << "Number of serial ports:" << QSerialPortInfo::availablePorts().count();
    foreach (const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
    {
        qDebug() << "\nPort:" << serialPortInfo.portName();
        ui->comboBox_portsName->clear();
        ui->comboBox_portsName->addItem(serialPortInfo.portName());
    }
}

void MainWindow::on_pushButton_portOpen_clicked()
{
    m_serialPortSensor->setPortName(ui->comboBox_portsName->currentText());
    m_serialPortSensor->setParity(QSerialPort::NoParity);
    m_serialPortSensor->setBaudRate(QSerialPort::Baud9600, QSerialPort::AllDirections);
    m_serialPortSensor->setStopBits(QSerialPort::OneStop);
    m_serialPortSensor->setFlowControl(QSerialPort::NoFlowControl);

    m_serialPortSensor->open(QIODevice::ReadWrite);

    if (m_serialPortSensor->isOpen())
    {
        qDebug() << "Serial port is open...";
        connect(m_serialPortSensor, SIGNAL(readyRead()), this, SLOT(readyRead()));
    }
    else
    {
        qDebug() << "OPEN ERROR: " << m_serialPortSensor->errorString();
    }
}

void MainWindow::on_pushButton_portClose_clicked()
{
    m_serialPortSensor->close();
    if (not m_serialPortSensor->isOpen())
    {
        qDebug() << "...serial port is closed!";
    }
}

void MainWindow::readyRead()
{
    QByteArray byteArray = m_serialPortSensor->readAll();
    if(byteArray.size() <= 0) return;
    QList<QByteArray> results = byteArray.split('\r');
    for(QByteArray result : results)
    {
        QString value = result.trimmed();
        if(value.isEmpty()) continue;
//                qDebug() << value;

        QStringList allParams = value.split(' ');
        for(QString param : allParams)
        {
            QStringList key_value = param.split(':');

            if(key_value.at(0) == "ED")
            {
                if(m_switchN14 == 2)
                {
                    m_encoderBottom = (key_value.at(1).toInt())/4-1;
//                    qDebug() << "ED: " << m_encoderBottom;
                }
            }
            else if(key_value.at(0) == "EG")
            {
                if(m_switchN14 == 2)
                {
                    m_encoderUpper = (key_value.at(1).toInt())/4-1;
//                    qDebug() << "EG: " << m_encoderUpper;
                }
            }
            else if(key_value.at(0) == "P14")
            {
                m_switchN14 = key_value.at(1).toInt();
//                qDebug() << "P14: " << m_switchN14;

                if(m_switchN14 != 3)
                {
                    m_option3Time.restart();
                }
            }
        }
    }
}

char MainWindow::getLeftRpm()
{
    char rpm = 0;
    rpm |= ui->rpm0->isChecked() << 7;
    rpm |= ui->rpm1->isChecked() << 6;
    rpm |= ui->rpm2->isChecked() << 5;
    rpm |= ui->rpm3->isChecked() << 4;
    rpm |= ui->rpm4->isChecked() << 3;
    rpm |= ui->rpm5->isChecked() << 2;
    rpm |= ui->rpm6->isChecked() << 1;
    rpm |= ui->rpm7->isChecked();

    return rpm;
}

char MainWindow::getRightRpm()
{
    char rpm = 0;
    rpm |= ui->rpm8->isChecked() << 7;
    rpm |= ui->rpm9->isChecked() << 6;
    rpm |= ui->rpm10->isChecked() << 5;
    rpm |= ui->rpm11->isChecked() << 4;
    rpm |= ui->rpm12->isChecked() << 3;
    rpm |= ui->rpm13->isChecked() << 2;
    rpm |= ui->rpm14->isChecked() << 1;

    return rpm;
}

char MainWindow::getFlags()
{
    char flags = 0;
    flags |= ui->flagBlueLeft->isChecked()    << 7;
    flags |= ui->flagBlueRight->isChecked()   << 6;
    flags |= ui->flagRedLeft->isChecked()     << 5;
    flags |= ui->flagRedRight->isChecked()    << 4;
    flags |= ui->flagGreenLeft->isChecked()   << 3;
    flags |= ui->flagGreenRight->isChecked()  << 2;
    flags |= ui->flagYellowLeft->isChecked();
    flags |= ui->flagYellowRight->isChecked() << 1;

    return flags;
}

void MainWindow::updateRpm(int p_rpm)
{
    static QRadioButton *rpmButton[15] = {ui->rpm0, ui->rpm1, ui->rpm2, ui->rpm3, ui->rpm4, ui->rpm5, ui->rpm6, ui->rpm7, ui->rpm8, ui->rpm9, ui->rpm10, ui->rpm11, ui->rpm12, ui->rpm13, ui->rpm14};

    int i = 0;
    for(QRadioButton *rpm : rpmButton)
    {
        rpm->setChecked(i < p_rpm ? true : false);
        i++;
    }
}

void MainWindow::on_pushButton_clicked()
{
    char symbols[21] = {};
    char encoderUpper[4] = {};
    char encoderBottom[4] = {};
    unsigned int rpm = 0xffff;

    sprintf(encoderUpper, "%4d", m_encoderUpper);
    sprintf(encoderBottom, "%4d", m_encoderBottom);

    if(m_switchN14 == 1)
    {
        symbols[4] = displayNumber[0];
        symbols[5] = '.';
        symbols[6] = displayNumber[1];
        symbols[7] = '.';
        symbols[8] = displayNumber[2];
        symbols[9] = '.';
        symbols[10] = displayNumber[3];
        symbols[11] = '.';

        symbols[12] = displayNumber[4];
        symbols[13] = '.';
        symbols[14] = displayNumber[5];
        symbols[15] = '.';
        symbols[16] = displayNumber[6];
        symbols[17] = '.';
        symbols[18] = displayNumber[7];
        symbols[19] = '.';
    }
    else if(m_switchN14 == 2)
    {
        symbols[4] = encoderBottom[0];
        symbols[5] = ' ';
        symbols[6] = encoderBottom[1];
        symbols[7] = ' ';
        symbols[8] = encoderBottom[2];
        symbols[9] = ' ';
        symbols[10] = encoderBottom[3];
        symbols[11] = ' ';

        symbols[12] = encoderUpper[0];
        symbols[13] = ' ';
        symbols[14] = encoderUpper[1];
        symbols[15] = ' ';
        symbols[16] = encoderUpper[2];
        symbols[17] = ' ';
        symbols[18] = encoderUpper[3];
        symbols[19] = ' ';
    }
    else if(m_switchN14 == 3)
    {
        QString time = QTime::fromMSecsSinceStartOfDay(m_option3Time.elapsed()).toString("m.ss.zzz");
        //qDebug() << time;
        symbols[12] = time[0].toLatin1();
        symbols[13] = time[1].toLatin1();
        symbols[14] = time[2].toLatin1();
        symbols[15] = ' ';
        symbols[16] = time[3].toLatin1();
        symbols[17] = time[4].toLatin1();
        symbols[18] = time[5].toLatin1();
        symbols[19] = ' ';
    }
    else
    {
        symbols[0] = getLeftRpm();
        symbols[1] = getRightRpm();

        symbols[2] = getFlags();
        symbols[3] = ui->gear->currentText()[0].toLatin1();

        symbols[4] = ui->wys0->currentText()[0].toLatin1();
        symbols[5] = '.';
        symbols[6] = ui->wys1->currentText()[0].toLatin1();
        symbols[7] = '.';
        symbols[8] = ui->wys2->currentText()[0].toLatin1();
        symbols[9] = '.';
        symbols[10] = ui->wys3->currentText()[0].toLatin1();
        symbols[11] = '.';

        symbols[12] = ui->wys4->currentText()[0].toLatin1();
        symbols[13] = '.';
        symbols[14] = ui->wys5->currentText()[0].toLatin1();
        symbols[15] = '.';
        symbols[16] = ui->wys6->currentText()[0].toLatin1();
        symbols[17] = '.';
        symbols[18] = ui->wys7->currentText()[0].toLatin1();
        symbols[19] = '.';
    }

    symbols[20] = '\n';

    //char flag = 0xaa;
    //char symbolsTest[21] = {flag, flag, flag, 4, '1', '.', '3', '.', '5', '.', '7', '.', '7', '.', '5', '.', '3', '.', '1', '.', '\n'};
    if(m_serialPortSensor->isOpen())
    {
        m_serialPortSensor->write(symbols, sizeof(symbols));
    }
}

void MainWindow::on_pushButton_showUsbDevice_clicked()
{
    UsbProxy usb;
    usb.printUsbDevices();
}

void MainWindow::on_pushButton_2_clicked()
{
    UsbProxy usb;
    usb.sendData();
}

void MainWindow::on_rpm0_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm1_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm2_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm3_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm4_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm5_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm9_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm6_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm7_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm8_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm10_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm11_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm12_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm13_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_rpm14_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_flagBlueLeft_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_flagGreenLeft_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_flagRedLeft_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_flagYellowLeft_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_flagYellowRight_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_flagRedRight_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_flagGreenRight_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_flagBlueRight_toggled(bool)
{
    on_pushButton_clicked();
}

void MainWindow::on_gear_currentIndexChanged(int)
{
    on_pushButton_clicked();
}

void MainWindow::on_wys0_currentIndexChanged(int)
{
    on_pushButton_clicked();
}

void MainWindow::on_wys1_currentIndexChanged(int)
{
    on_pushButton_clicked();
}

void MainWindow::on_wys2_currentIndexChanged(int)
{
    on_pushButton_clicked();
}

void MainWindow::on_wys3_currentIndexChanged(int)
{
    on_pushButton_clicked();
}

void MainWindow::on_wys4_currentIndexChanged(int)
{
    on_pushButton_clicked();
}

void MainWindow::on_wys5_currentIndexChanged(int)
{
    on_pushButton_clicked();
}

void MainWindow::on_wys6_currentIndexChanged(int)
{
    on_pushButton_clicked();
}

void MainWindow::on_wys7_currentIndexChanged(int)
{
    on_pushButton_clicked();
}

void MainWindow::on_horizontalSlider_rpm_sliderMoved(int position)
{
    qDebug() << position;
    m_rpm = position;
    updateRpm(position);
}

void MainWindow::updateDashboard()
{
    on_pushButton_clicked();
}

void MainWindow::updateDisplay()
{
    char first = displayNumber[0];
    for(int i = 0; i < 7; i++)
    {
        displayNumber[i] = displayNumber[i+1];
    }
    displayNumber[7] = first;
//    qDebug() << displayNumber;
}
