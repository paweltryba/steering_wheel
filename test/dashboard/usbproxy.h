#ifndef USBPROXY_H
#define USBPROXY_H
#include "libusb.h"


class UsbProxy
{
public:
    UsbProxy();
    void printUsbDevices();
    void listAllUsbDevices();
    void sendData();
private:
    void print_bos(libusb_device_handle *handle);
    void print_interface(const libusb_interface *interface);
    void print_configuration(libusb_config_descriptor *config);
    int print_device(libusb_device *dev, int level);
    void print_ss_usb_cap(libusb_ss_usb_device_capability_descriptor *ss_usb_cap);
    void print_2_0_ext_cap(libusb_usb_2_0_extension_descriptor *usb_2_0_ext_cap);
    void print_altsetting(const libusb_interface_descriptor *interface);
    void print_endpoint(const libusb_endpoint_descriptor *endpoint);
    void print_endpoint_comp(const libusb_ss_endpoint_companion_descriptor *ep_comp);
    void list_devs(libusb_device **devs);

    int m_verbose = 0;
    int m_vid = 0x03eb;
    int m_pid = 0x2062;
    int usbSend(libusb_device_handle *handle, uint16_t packet);
};

#endif // USBPROXY_H
