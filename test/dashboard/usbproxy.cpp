#include <stdio.h>
#include <string.h>
#include "usbproxy.h"
#include <iostream>


UsbProxy::UsbProxy()
{

}

void UsbProxy::print_endpoint_comp(const struct libusb_ss_endpoint_companion_descriptor *ep_comp)
{
    printf("      USB 3.0 Endpoint Companion:\n");
    printf("        bMaxBurst:        %d\n", ep_comp->bMaxBurst);
    printf("        bmAttributes:     0x%02x\n", ep_comp->bmAttributes);
    printf("        wBytesPerInterval: %d\n", ep_comp->wBytesPerInterval);
}

void UsbProxy::print_endpoint(const struct libusb_endpoint_descriptor *endpoint)
{
    int i, ret;

    printf("      Endpoint:\n");
    printf("        bEndpointAddress: %02xh\n", endpoint->bEndpointAddress);
    printf("        bmAttributes:     %02xh\n", endpoint->bmAttributes);
    printf("        wMaxPacketSize:   %d\n", endpoint->wMaxPacketSize);
    printf("        bInterval:        %d\n", endpoint->bInterval);
    printf("        bRefresh:         %d\n", endpoint->bRefresh);
    printf("        bSynchAddress:    %d\n", endpoint->bSynchAddress);

    for (i = 0; i < endpoint->extra_length;) {
        if (LIBUSB_DT_SS_ENDPOINT_COMPANION == endpoint->extra[i + 1]) {
            struct libusb_ss_endpoint_companion_descriptor *ep_comp;

            ret = libusb_get_ss_endpoint_companion_descriptor(NULL, endpoint, &ep_comp);
            if (LIBUSB_SUCCESS != ret) {
                continue;
            }

            print_endpoint_comp(ep_comp);

            libusb_free_ss_endpoint_companion_descriptor(ep_comp);
        }

        i += endpoint->extra[i];
    }
}

void UsbProxy::print_altsetting(const struct libusb_interface_descriptor *interface)
{
    uint8_t i;

    printf("    Interface:\n");
    printf("      bInterfaceNumber:   %d\n", interface->bInterfaceNumber);
    printf("      bAlternateSetting:  %d\n", interface->bAlternateSetting);
    printf("      bNumEndpoints:      %d\n", interface->bNumEndpoints);
    printf("      bInterfaceClass:    %d\n", interface->bInterfaceClass);
    printf("      bInterfaceSubClass: %d\n", interface->bInterfaceSubClass);
    printf("      bInterfaceProtocol: %d\n", interface->bInterfaceProtocol);
    printf("      iInterface:         %d\n", interface->iInterface);

    for (i = 0; i < interface->bNumEndpoints; i++)
        print_endpoint(&interface->endpoint[i]);
}

void UsbProxy::print_2_0_ext_cap(struct libusb_usb_2_0_extension_descriptor *usb_2_0_ext_cap)
{
    printf("    USB 2.0 Extension Capabilities:\n");
    printf("      bDevCapabilityType: %d\n", usb_2_0_ext_cap->bDevCapabilityType);
    printf("      bmAttributes:       0x%x\n", usb_2_0_ext_cap->bmAttributes);
}

void UsbProxy::print_ss_usb_cap(struct libusb_ss_usb_device_capability_descriptor *ss_usb_cap)
{
    printf("    USB 3.0 Capabilities:\n");
    printf("      bDevCapabilityType: %d\n", ss_usb_cap->bDevCapabilityType);
    printf("      bmAttributes:       0x%x\n", ss_usb_cap->bmAttributes);
    printf("      wSpeedSupported:    0x%x\n", ss_usb_cap->wSpeedSupported);
    printf("      bFunctionalitySupport: %d\n", ss_usb_cap->bFunctionalitySupport);
    printf("      bU1devExitLat:      %d\n", ss_usb_cap->bU1DevExitLat);
    printf("      bU2devExitLat:      %d\n", ss_usb_cap->bU2DevExitLat);
}

void UsbProxy::print_bos(libusb_device_handle *handle)
{
    struct libusb_bos_descriptor *bos;
    int ret;

    ret = libusb_get_bos_descriptor(handle, &bos);
    if (0 > ret) {
        return;
    }

    printf("  Binary Object Store (BOS):\n");
    printf("    wTotalLength:       %d\n", bos->wTotalLength);
    printf("    bNumDeviceCaps:     %d\n", bos->bNumDeviceCaps);

    if(bos->dev_capability[0]->bDevCapabilityType == LIBUSB_BT_USB_2_0_EXTENSION) {

        struct libusb_usb_2_0_extension_descriptor *usb_2_0_extension;
            ret =  libusb_get_usb_2_0_extension_descriptor(NULL, bos->dev_capability[0],&usb_2_0_extension);
            if (0 > ret) {
                return;
            }

                print_2_0_ext_cap(usb_2_0_extension);
                libusb_free_usb_2_0_extension_descriptor(usb_2_0_extension);
        }

    if(bos->dev_capability[0]->bDevCapabilityType == LIBUSB_BT_SS_USB_DEVICE_CAPABILITY) {

            struct libusb_ss_usb_device_capability_descriptor *dev_cap;
        ret = libusb_get_ss_usb_device_capability_descriptor(NULL, bos->dev_capability[0],&dev_cap);
            if (0 > ret) {
                return;
            }

            print_ss_usb_cap(dev_cap);
            libusb_free_ss_usb_device_capability_descriptor(dev_cap);
        }

    libusb_free_bos_descriptor(bos);
}

void UsbProxy::print_interface(const struct libusb_interface *interface)
{
    int i;

    for (i = 0; i < interface->num_altsetting; i++)
        print_altsetting(&interface->altsetting[i]);
}

void UsbProxy::print_configuration(struct libusb_config_descriptor *config)
{
    uint8_t i;

    printf("  Configuration:\n");
    printf("    wTotalLength:         %d\n", config->wTotalLength);
    printf("    bNumInterfaces:       %d\n", config->bNumInterfaces);
    printf("    bConfigurationValue:  %d\n", config->bConfigurationValue);
    printf("    iConfiguration:       %d\n", config->iConfiguration);
    printf("    bmAttributes:         %02xh\n", config->bmAttributes);
    printf("    MaxPower:             %d\n", config->MaxPower);

    for (i = 0; i < config->bNumInterfaces; i++)
        print_interface(&config->interface[i]);
}

int UsbProxy::print_device(libusb_device *dev, int level)
{
    struct libusb_device_descriptor desc;
    libusb_device_handle *handle = NULL;
    char description[256];
    unsigned char string[256];
    int ret;
    uint8_t i;

    ret = libusb_get_device_descriptor(dev, &desc);
    if (ret < 0) {
        fprintf(stderr, "failed to get device descriptor");
        return -1;
    }

    ret = libusb_open(dev, &handle);
    if (LIBUSB_SUCCESS == ret) {
        if (desc.iManufacturer) {
            ret = libusb_get_string_descriptor_ascii(handle, desc.iManufacturer, string, sizeof(string));
            if (ret > 0)
                snprintf(description, sizeof(description), "%s - ", string);
            else
                snprintf(description, sizeof(description), "%04X - ",
                desc.idVendor);
        }
        else
            snprintf(description, sizeof(description), "%04X - ",
            desc.idVendor);

        if (desc.iProduct) {
            ret = libusb_get_string_descriptor_ascii(handle, desc.iProduct, string, sizeof(string));
            if (ret > 0)
                snprintf(description + strlen(description), sizeof(description) -
                strlen(description), "%s", string);
            else
                snprintf(description + strlen(description), sizeof(description) -
                strlen(description), "%04X", desc.idProduct);
        }
        else
            snprintf(description + strlen(description), sizeof(description) -
            strlen(description), "%04X", desc.idProduct);
    }
    else {
        snprintf(description, sizeof(description), "%04X - %04X",
            desc.idVendor, desc.idProduct);
    }

    printf("%.*sDev (bus %d, device %d): %s\n", level * 2, "                    ",
        libusb_get_bus_number(dev), libusb_get_device_address(dev), description);

    if (handle && m_verbose) {
        if (desc.iSerialNumber) {
            ret = libusb_get_string_descriptor_ascii(handle, desc.iSerialNumber, string, sizeof(string));
            if (ret > 0)
                printf("%.*s  - Serial Number: %s\n", level * 2,
                "                    ", string);
        }
    }

    if (m_verbose) {
        for (i = 0; i < desc.bNumConfigurations; i++) {
            struct libusb_config_descriptor *config;
            ret = libusb_get_config_descriptor(dev, i, &config);
            if (LIBUSB_SUCCESS != ret) {
                printf("  Couldn't retrieve descriptors\n");
                continue;
            }

            print_configuration(config);

            libusb_free_config_descriptor(config);
        }

        if (handle && desc.bcdUSB >= 0x0201) {
            print_bos(handle);
        }
    }

    if (handle)
        libusb_close(handle);

    return 0;
}

void UsbProxy::list_devs(libusb_device **devs)
{
    libusb_device *dev;
    int i = 0, j = 0;
    uint8_t path[8];

    while ((dev = devs[i++]) != NULL) {
        struct libusb_device_descriptor desc;
        int r = libusb_get_device_descriptor(dev, &desc);
        if (r < 0) {
            fprintf(stderr, "failed to get device descriptor");
            return;
        }

        printf("%04x:%04x (bus %d, device %d)",
            desc.idVendor, desc.idProduct,
            libusb_get_bus_number(dev), libusb_get_device_address(dev));

        r = libusb_get_port_numbers(dev, path, sizeof(path));
        if (r > 0) {
            printf(" path: %d", path[0]);
            for (j = 1; j < r; j++)
                printf(".%d", path[j]);
        }
        printf("\n");
    }
}

void UsbProxy::printUsbDevices()
{
    libusb_device **devs;
    ssize_t cnt;
    int r, i;

    m_verbose = 1;

    r = libusb_init(NULL);
    if (r < 0)
    {
        return;
    }

    cnt = libusb_get_device_list(NULL, &devs);
    if (cnt < 0)
    {
        return;
    }

    for (i = 0; devs[i]; ++i)
    {
        print_device(devs[i], 0);
    }

    libusb_free_device_list(devs, 1);

    libusb_exit(NULL);
    return;
}

void UsbProxy::listAllUsbDevices()
{
    libusb_device **devs;
    int r;
    ssize_t cnt;

    r = libusb_init(NULL);
    if (r < 0)
    {
        return;
    }

    cnt = libusb_get_device_list(NULL, &devs);
    if (cnt < 0)
    {
        return;
    }

    list_devs(devs);
    libusb_free_device_list(devs, 1);

    libusb_exit(NULL);
    return;
}

void cb_out(struct libusb_transfer *transfer)
{
    fprintf(stderr, "cb_out: status =%d, actual_length=%d\n", transfer->status, transfer->actual_length);
}

void UsbProxy::sendData()
{
    std::cout<<"sendData"<<std::endl;
    int rc;
    static struct libusb_device_handle *devh = NULL;

    rc = libusb_init(NULL);
    if (rc < 0)
    {
        fprintf(stderr, "Error initializing libusb: %s\n", libusb_error_name(rc));
        exit(1);
    }

    libusb_set_option(NULL, LIBUSB_OPTION_LOG_LEVEL, 3);

    devh = libusb_open_device_with_vid_pid(NULL, m_vid, m_pid);
    if (!devh)
    {
        std::cout<<"Cannot open device"<<std::endl;
        return;
    }
    else
    {
        std::cout<<"Device Opened"<<std::endl;
    }

  //  libusb_control_transfer(devh,








//    for (int if_num = 0; if_num < 3; if_num++)
//    {
//        if (libusb_kernel_driver_active(devh, if_num))
//        {
//            libusb_detach_kernel_driver(devh, if_num);
//        }
//        rc = libusb_claim_interface(devh, if_num);
//        if(rc < 0)
//        {
//            std::cout<<"Error claiming interface: " << if_num << " result: " << rc << std::endl;
//            //return;
//        }
//        else
//        {
//            std::cout<<"Claimed interface: " << if_num << " result: " << rc << std::endl;
//        }
//    }

//    unsigned char data[21] = {0xff, 0xff, 0xff, 6, '1', '.', '3', '.', '5', '.', '7', '.', '7', '.', '5', '.', '3', '.', '1', '.', '\n'};
//    int actual;

//    rc = libusb_bulk_transfer(devh, (0x82 | LIBUSB_ENDPOINT_IN), data, 21, &actual, 0);
//    if(rc == 0 && actual == 21)
//    {
//        std::cout<<"Writing Successful!"<<std::endl;
//    }
//    else
//    {
//        std::cout<<"Write Error"<<std::endl;
//    }

//    rc = libusb_bulk_transfer(devh, (0x04 | LIBUSB_ENDPOINT_IN), data, 21, &actual, 0);
//    if(rc == 0 && actual == 21)
//    {
//        std::cout<<"Writing Successful!"<<std::endl;
//    }
//    else
//    {
//        std::cout<<"Write Error"<<std::endl;
//    }

//    rc = libusb_bulk_transfer(devh, (0x83 | LIBUSB_ENDPOINT_IN), data, 21, &actual, 0);
//    if(rc == 0 && actual == 21)
//    {
//        std::cout<<"Writing Successful!"<<std::endl;
//    }
//    else
//    {
//        std::cout<<"Write Error"<<std::endl;
//    }

//    rc = libusb_bulk_transfer(devh, (0x81 | LIBUSB_ENDPOINT_IN), data, 21, &actual, 0);
//    if(rc == 0 && actual == 21)
//    {
//        std::cout<<"Writing Successful!"<<std::endl;
//    }
//    else
//    {
//        std::cout<<"Write Error Zapisano: " << actual <<std::endl;
//    }








//    libusb_device **devs;
//    libusb_device_handle *dev_handle;
//    libusb_context *ctx = NULL;
//    int r;

//    libusb_init(NULL);
//    dev_handle = libusb_open_device_with_vid_pid(ctx, m_vid, m_pid);

//    if(dev_handle == NULL)
//    {
//        std::cout<<"Cannot open device"<<std::endl;
//    }
//    else
//    {
//        std::cout<<"Device Opened"<<std::endl;
//    }

//    for (int if_num = 0; if_num < 2; if_num++)
//    {
//        if (libusb_kernel_driver_active(dev_handle, if_num))
//        {
//            libusb_detach_kernel_driver(dev_handle, if_num);
//        }
//        r = libusb_claim_interface(dev_handle, if_num);
//        if (r < 0)
//        {
//            fprintf(stderr, "Error claiming interface: %s\n", libusb_error_name(r));
//            break;
//        }
//    }

    /*

    unsigned char data[21] = {0xff, 0xff, 0xff, 6, '1', '.', '3', '.', '5', '.', '7', '.', '7', '.', '5', '.', '3', '.', '1', '.', '\n'};
    int actual;
    if(libusb_kernel_driver_active(dev_handle, 0) == 1)
    {
        std::cout<<"Kernel Driver Active"<<std::endl;
        if(libusb_detach_kernel_driver(dev_handle, 0) == 0)
        {
            std::cout<<"Kernel Driver Detached!"<<std::endl;
        }
    }
    else
    {
        std::cout<<"Kernel Driver Active"<<std::endl;
    }

    */
/*
    r = libusb_claim_interface(dev_handle, 0); //claim interface 0 (the first) of device (mine had jsut 1)

    if(r < 0)
    {
        std::cout<<"Cannot Claim Interface"<<std::endl;
        return;
    }

    std::cout<<"Claimed Interface"<<std::endl;
    std::cout<<"Data->"<<data<<"<-"<<std::endl; //just to see the data we want to write : abcd
    std::cout<<"Writing Data..."<<std::endl;

    r = libusb_bulk_transfer(dev_handle, (0x81 | LIBUSB_ENDPOINT_OUT), data, 21, &actual, 0); //my device's out endpoint was 2, found with trial- the device had 2 endpoints: 2 and 129

    if(r == 0 && actual == 21)
    {

        std::cout<<"Writing Successful!"<<std::endl;
    }

    else
    {
        std::cout<<"Write Error"<<std::endl;

    }

    r = libusb_release_interface(dev_handle, 0); //release the claimed interface

    if(r!=0)
    {

        std::cout<<"Cannot Release Interface"<<std::endl;

        return;

    }

    std::cout<<"Released Interface"<<std::endl;

    libusb_close(dev_handle); //close the device we opened

    libusb_exit(ctx); //needs to be called to end the
    */
}
