/*
 * spi.h
 *
 * Created: 2014-02-27 12:23:41
 *  Author: Pawel
 */ 


#ifndef SPI_H_
#define SPI_H_
#include <avr/io.h>

#define LAT PIN3_bm
#define MOSI PIN5_bm
#define SCK_SPI PIN7_bm
#define SS PIN4_bm
#define LAT_ON PORTC.OUTSET = LAT
#define LAT_OFF PORTC.OUTCLR = LAT

void spi_init()
{
	PORTC.DIRSET = SS; //SS OUT
	PORTC.OUTSET = SS; //HIGH
	PORTC.DIRSET = LAT | MOSI | SCK_SPI; //wszystko na OUT
	PORTC.OUTCLR = LAT | MOSI | SCK_SPI; //wszystko na OUT LOW
	
	SPIC.CTRL |= SPI_ENABLE_bm; //w��cz SPI
	SPIC.CTRL |= SPI_MASTER_bm; //jako master
	SPIC.CTRL |= SPI_DORD_bm;   //LSB jest transmitowany pierwszy
	
	SPIC.CTRL |= SPI_PRESCALER_DIV4_gc; //narazie nie wiem jaki to ma wp�yw
	
	
}

void spi_zapisz(int16_t wyswietlacz, int16_t segment) /*segment - odpowiednia pozycja 1 w zmiennej zalacza dany wyswietlacz
													   *wyswietlacz - poszczegolne bity zalaczaja poszczegolne segmenty pojedynczego wy�wietlacza 14 segmentowego LED*/
{
	SPIC.DATA = wyswietlacz;
	while(!(SPIC.STATUS & SPI_IF_bm));

	SPIC.DATA = wyswietlacz>>8;
	while(!(SPIC.STATUS & SPI_IF_bm));

	SPIC.DATA = segment;
	while(!(SPIC.STATUS & SPI_IF_bm));

	SPIC.DATA = segment>>8;
	while(!(SPIC.STATUS & SPI_IF_bm));

	LAT_ON;
	LAT_OFF;
}


#endif /* SPI_H_ */