/*
 * timer1s.h
 *
 * Created: 2014-03-01 20:22:45
 *  Author: Pawel
 */ 


#ifndef TIMER1S_H_
#define TIMER1S_H_
#include <avr/io.h>

void timer1s_init(void)
{
	TCC1.CTRLA = TC_CLKSEL_DIV1024_gc;
	
	TCC1.PER = 30000; //aby przerwanie by�o z f = 1 Hz gdy F_CPU=32MHz
	//TCC1.INTCTRLA = TC_OVFINTLVL_HI_gc; //START
}



#endif /* TIMER1S_H_ */