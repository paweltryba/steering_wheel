/*
             LUFA Library
     Copyright (C) Dean Camera, 2013.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2013  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

/** \file
 *
 *  Main source file for the VirtualSerialMouse demo. This file contains the main tasks of
 *  the demo and is responsible for the initial application hardware configuration.
 */

#include "VirtualSerialMouse.h"
#include "dashboard.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include "klawiatura.h"
#include "pwm.h"
#include "timer1s.h"
#include "LUFA/Drivers/Peripheral/Serial.h"
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <stddef.h>
#include <util/atomic.h>
#include "enkoder.h"


/** LUFA CDC Class driver interface configuration and state information. This structure is
 *  passed to all CDC Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface =
	{
		.Config =
			{
				.ControlInterfaceNumber         = 0,
				.DataINEndpoint                 =
					{
						.Address                = CDC_TX_EPADDR,
						.Size                   = CDC_TXRX_EPSIZE,
						.Banks                  = 1,
					},
				.DataOUTEndpoint                =
					{
						.Address                = CDC_RX_EPADDR,
						.Size                   = CDC_TXRX_EPSIZE,
						.Banks                  = 1,
					},
				.NotificationEndpoint           =
					{
						.Address                = CDC_NOTIFICATION_EPADDR,
						.Size                   = CDC_NOTIFICATION_EPSIZE,
						.Banks                  = 1,
					},
			},
	};

/** Buffer to hold the previously generated Mouse HID report, for comparison purposes inside the HID class driver. */
static uint8_t PrevMouseHIDReportBuffer[sizeof(USB_MouseReport_Data_t)];

/** LUFA HID Class driver interface configuration and state information. This structure is
 *  passed to all HID Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_HID_Device_t Mouse_HID_Interface =
	{
		.Config =
			{
				.InterfaceNumber                = 2,
				.ReportINEndpoint               =
					{
						.Address                = MOUSE_EPADDR,
						.Size                   = MOUSE_EPSIZE,
						.Banks                  = 1,
					},
				.PrevReportINBuffer             = PrevMouseHIDReportBuffer,
				.PrevReportINBufferSize         = sizeof(PrevMouseHIDReportBuffer),
			},
	};


/** Main program entry point. This routine contains the overall program flow, including initial
 *  setup of all components and the main program loop.
 */
#define DOUT PIN0_bm //input
#define SCK_SCALE PIN1_bm  //output
#define SCALE_PORT PORTD

#define GET_DOUT ((SCALE_PORT.IN & DOUT) != 0)
#define SET_SCK_SCALE (SCALE_PORT.OUTSET |= SCK_SCALE)
#define CLEAR_SCK_SCALE (SCALE_PORT.OUTCLR |= SCK_SCALE)

void setScales()
{
	SCALE_PORT.DIRSET = SCK_SCALE;  //SCK OUTPUT
	SCALE_PORT.DIRCLR = DOUT; //DDO INPUT
	
	SET_SCK_SCALE;
	_delay_us(1);
	CLEAR_SCK_SCALE;
}
unsigned long readScale()
{
	unsigned long count = 0;
	char i;

	while(GET_DOUT);
	_delay_us(1);
	for(i = 0; i < 24; i++)
	{
		SET_SCK_SCALE;
		_delay_us(1);
		count = count << 1;
		if(GET_DOUT) count++;
		CLEAR_SCK_SCALE;
		_delay_us(1);
	}
	
	SET_SCK_SCALE;
	_delay_us(2);
	CLEAR_SCK_SCALE;
	return count;
}

uint8_t ReadCalibrationByte( uint8_t index )
{
	uint8_t result;
	
	/* Load the NVM Command register to read the calibration row. */
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	result = pgm_read_byte(index);
	
	/* Clean up NVM Command register. */
	NVM_CMD = NVM_CMD_NO_OPERATION_gc;
	
	return( result );
}

void initAdc()
{
	ADCA.PRESCALER = ADC_PRESCALER_DIV512_gc;
	ADCA.INTFLAGS = ADC_CH0IF_bm;// | ADC_CH1IF_bm | ADC_CH2IF_bm | ADC_CH3IF_bm;
	ADCA.CTRLA = ADC_ENABLE_bm;
	ADCA.CTRLB = ADC_FREERUN_bm | ADC_RESOLUTION_8BIT_gc;
	ADCA.EVCTRL = ADC_SWEEP_0123_gc;
	
	//PORTA.DIRCLR = PIN0_bm;
	PORTA.DIRCLR = PIN1_bm;
	PORTA.DIRCLR = PIN2_bm;
	PORTA.DIRCLR = PIN3_bm;
	PORTA.DIRCLR = PIN4_bm;
	
	//ADCA.REFCTRL = ADC_REFSEL_AREFA_gc;
	ADCA.REFCTRL = ADC_REFSEL_INT1V_gc;
	
	ADCA.CH0.CTRL = ADC_CH_INPUTMODE0_bm;
	ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN2_gc; //przelacznik: [13:PORTA_2]
	ADCA.CH0.INTCTRL = ADC_CH_INTLVL1_bm | ADC_CH_INTLVL0_bm;
	
	ADCA.CH1.CTRL = ADC_CH_INPUTMODE0_bm;
	ADCA.CH1.MUXCTRL = ADC_CH_MUXPOS_PIN3_gc; //przelacznik: [14:PORTA_3]
	ADCA.CH1.INTCTRL = ADC_CH_INTLVL1_bm | ADC_CH_INTLVL0_bm;

	ADCA.CH2.CTRL = ADC_CH_INPUTMODE0_bm;
	ADCA.CH2.MUXCTRL = ADC_CH_MUXPOS_PIN4_gc; //przelacznik: [15:PORTA_4]
	ADCA.CH2.INTCTRL = ADC_CH_INTLVL1_bm | ADC_CH_INTLVL0_bm;
	
	ADCA.CH3.CTRL = ADC_CH_INPUTMODE0_bm;
	ADCA.CH3.MUXCTRL = ADC_CH_MUXPOS_PIN1_gc; //przelacznik: [16:PORTA_1]
	ADCA.CH3.INTCTRL = ADC_CH_INTLVL1_bm | ADC_CH_INTLVL0_bm;
	
	ADCA.CALL = ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, ADCACAL0) );
	ADCA.CALH = ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, ADCACAL1) );
}

#define ENCODER_GORA_TIMER TCD0
#define ENCODER_GORA ENCODER_GORA_TIMER.CNT

#define ENCODER_DOL_TIMER TCE0
#define ENCODER_DOL ENCODER_DOL_TIMER.CNT

char bufResult[100] = {};
volatile int switch13 = 0;
volatile int switch14 = 0;
volatile int switch15 = 0;
volatile int switch16 = 0;
int switch13Avg = 0;
int switch14Avg = 0;
int switch15Avg = 0;
int switch16Avg = 0;

int switch13Stable = -1;
int switch14Stable = -1;
int switch15Stable = -1;
int switch16Stable = -1;

int gearDownOld = -1;
int gearUpOld = -1;

int encoderUpOld = -1;
int encoderDownOld = -1;

bool isNewData = false;

int gear = 0;

ISR(ADCA_CH0_vect)
{
	switch13 = ADCA.CH0RES;
}

ISR(ADCA_CH1_vect)
{
	switch14 = ADCA.CH1RES;
}

ISR(ADCA_CH2_vect)
{
	switch15 = ADCA.CH2RES;
}

ISR(ADCA_CH3_vect)
{
	switch16 = ADCA.CH3RES;
}

void configQDECGora()
{
	PORTD.DIRCLR = PIN4_bm; //syg A QDPH0
	PORTD.DIRCLR = PIN5_bm; //syg B QDPH90
	PORTD.PIN4CTRL = PORT_ISC_LEVEL_gc | PORT_OPC_PULLUP_gc;
	PORTD.PIN5CTRL = PORT_ISC_LEVEL_gc | PORT_OPC_PULLUP_gc;
	
	EVSYS.CH0MUX = EVSYS_CHMUX_PORTD_PIN4_gc;
	EVSYS.CH0CTRL = EVSYS_QDEN_bm | EVSYS_DIGFILT_4SAMPLES_gc;
	ENCODER_GORA_TIMER.PER = 0xffff;
	ENCODER_GORA_TIMER.CTRLA = TC_CLKSEL_DIV1_gc;
	ENCODER_GORA_TIMER.CTRLD = TC_EVACT_QDEC_gc | TC_EVSEL_CH0_gc;
}

void configQDECDol()
{
	PORTD.DIRCLR = PIN0_bm; //syg A QDPH0
	PORTD.DIRCLR = PIN1_bm; //syg B QDPH90
	PORTD.PIN0CTRL = PORT_ISC_LEVEL_gc | PORT_OPC_PULLUP_gc;
	PORTD.PIN1CTRL = PORT_ISC_LEVEL_gc | PORT_OPC_PULLUP_gc;
	
	EVSYS.CH2MUX = EVSYS_CHMUX_PORTD_PIN0_gc;
	EVSYS.CH2CTRL = EVSYS_QDEN_bm | EVSYS_DIGFILT_4SAMPLES_gc;
	ENCODER_DOL_TIMER.PER = 0xffff;
	ENCODER_DOL_TIMER.CTRLA = TC_CLKSEL_DIV1_gc;
	ENCODER_DOL_TIMER.CTRLD = TC_EVACT_QDEC_gc | TC_EVSEL_CH2_gc;
}

void configButtons()
{   		 //PORT //PIN
	PRZYCISK(PORTE, 0) //1
	PRZYCISK(PORTE, 1) //2

	PRZYCISK(PORTE, 3) //3
	PRZYCISK(PORTE, 2) //4
	
	PRZYCISK(PORTC, 0) //5
	PRZYCISK(PORTB, 3) //6
	PRZYCISK(PORTB, 2) //7
	PRZYCISK(PORTC, 1) //8
	
	PRZYCISK(PORTB, 1) //9
	PRZYCISK(PORTB, 0) //10
	
	PRZYCISK(PORTR, 1) //17
	PRZYCISK(PORTR, 0) //18
	
	PRZYCISK(PORTA, 7) //25
	PRZYCISK(PORTD, 2) //26
}

int zamienPrzelacznikNaOpcje(uint8_t przelacznik)
{
	if(przelacznik < 19)
	{
		return 1;
	}
	else if(przelacznik < 38)
	{
		return 2;
	}
	else if(przelacznik < 61)
	{
		return 3;
	}
	else if(przelacznik < 81)
	{
		return 4;
	}
	else if(przelacznik < 102)
	{
		return 5;
	}
	else if(przelacznik < 123)
	{
		return 6;
	}
	else if(przelacznik < 143)
	{
		return 7;
	}
	else if(przelacznik < 164)
	{
		return 8;
	}
	else if(przelacznik < 185)
	{
		return 9;
	}
	else if(przelacznik < 206)
	{
		return 10;
	}
	else if(przelacznik < 226)
	{
		return 11;
	}
	
	return 12;
}

void calculateSwitchesAndSendDataToCDC()
{
	static uint16_t loop = 0;
	loop++;
	
	switch13Avg = (switch13Avg*8+switch13*2)/10;
	switch14Avg = (switch14Avg*8+switch14*2)/10;
	switch15Avg = (switch15Avg*8+switch15*2)/10;
	switch16Avg = (switch16Avg*8+switch16*2)/10;
	
	if(loop % 20)
	{
		int gearUpNew = PRZYCISK_26;
		if(gearUpOld != gearUpNew)
		{
			gearUpOld = gearUpNew;
			gear += gearUpNew;
		}
		
		int gearDownNew = PRZYCISK_25;
		if(gearDownOld != gearDownNew)
		{
			gearDownOld = gearDownNew;
			gear -= gearDownNew;
		}
	}
	
	//\033\143
	if(loop >= 200)
	{
		switch13Stable = zamienPrzelacznikNaOpcje(switch13Avg);
		switch14Stable = zamienPrzelacznikNaOpcje(switch14Avg);
		switch15Stable = zamienPrzelacznikNaOpcje(switch15Avg);
		switch16Stable = zamienPrzelacznikNaOpcje(switch16Avg);
		
		sprintf(bufResult, "\033\143P13:%d P14:%d P15:%d P16:%d GD:%d GU:%d EG:%d ED:%d\n\r",
		switch13Stable,
		switch14Stable,
		switch15Stable,
		switch16Stable,
		gearDownOld,
		gearUpOld,
		ENCODER_GORA, 
		ENCODER_DOL);
		
		CDC_Device_SendString(&VirtualSerial_CDC_Interface, bufResult);
		loop = 0;
	}
}

void receiveFromCDC()
{
	int16_t byte_received = CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
	if(byte_received != -1)
	{
		//CDC_Device_SendByte(&VirtualSerial_CDC_Interface, byte_received);
		odb[++ile_odebrano] = byte_received; // Read data from the RX buffer
		//jesli odebrano wszystkie znaki to dajemy flage
		if ((odb[ile_odebrano] == '\n') || (ile_odebrano == ROZM_BUFORU - 1)) //odbiera do otrzymania znaku '\n' lub ROZM_BUFORU
		{
			odb[ile_odebrano] = '\0';
			flaga = 1;
		}

		if(flaga == 1)
		{
			display.rpm = (odb[0]<<8|odb[1]);
			display.flagi = odb[2];
			display.bieg = odb[3];

			display.segmenty[0][0] = odb[4];
			display.segmenty[0][1] = odb[5];

			display.segmenty[1][0] = odb[6];
			display.segmenty[1][1] = odb[7];

			display.segmenty[2][0] = odb[8];
			display.segmenty[2][1] = odb[9];

			display.segmenty[3][0] = odb[10];
			display.segmenty[3][1] = odb[11];

			display.segmenty[4][0] = odb[12];
			display.segmenty[4][1] = odb[13];

			display.segmenty[5][0] = odb[14];
			display.segmenty[5][1] = odb[15];

			display.segmenty[6][0] = odb[16];
			display.segmenty[6][1] = odb[17];

			display.segmenty[7][0] = odb[18];
			display.segmenty[7][1] = odb[19];

			ile_odebrano = -1;
			flaga = 0;
		}
	}
}

#if 0
void checkTypeSize()
{
	char bufResult[200] = {};
	int size = sprintf(bufResult, "sizeof(char):%d sizeof(int):%d sizeof(long):%d sizeof(unsigned long):%d sizeof(long long):%d sizeof(unsigned long long):%d\n\r",
	sizeof(char),
	sizeof(int),
	sizeof(long),
	sizeof(unsigned long),
	sizeof(long long),
	sizeof(unsigned long long));
	CDC_Device_SendString(&VirtualSerial_CDC_Interface, bufResult);
	//sizeof(char):1 sizeof(int):2 sizeof(long):4 sizeof(unsigned long):4 sizeof(long long):8 sizeof(unsigned long long):8		
}
#endif

int main(void)
{
	SetupHardware();
	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
	GlobalInterruptEnable();

	dashboard_init();
	initAdc();
	configQDECGora();
	configQDECDol();
	configButtons();
	
	for (;;)
	{
		calculateSwitchesAndSendDataToCDC();
		
		receiveFromCDC();
		CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
		HID_Device_USBTask(&Mouse_HID_Interface);
		USB_USBTask();
	}
}

ISR(TCC0_OVF_vect)
{
	switch(display.numer_wyswietlacza)
	{
		case I: //pierwszy od lewej
		wys = 0x8000>>display.numer_wyswietlacza;
		display.numer_wyswietlacza++;
		seg = znakNa16seg(display.segmenty[0][0],display.segmenty[0][1]);

		break;
		case II:
		wys = 0x8000>>display.numer_wyswietlacza;
		display.numer_wyswietlacza++;

		seg = znakNa16seg(display.segmenty[1][0],display.segmenty[1][1]);
		break;
		case III:
		wys = 0x8000>>display.numer_wyswietlacza;
		display.numer_wyswietlacza++;

		seg = znakNa16seg(display.segmenty[2][0],display.segmenty[2][1]);
		break;
		case IV:
		wys = 0x8000>>display.numer_wyswietlacza;
		display.numer_wyswietlacza++;

		seg = znakNa16seg(display.segmenty[3][0],display.segmenty[3][1]);
		break;
		case V: //biegi
		wys = 0x8000>>display.numer_wyswietlacza;
		display.numer_wyswietlacza++;

		seg = znakNa7seg(display.bieg);
		break;
		case VI:
		wys = 0x8000>>display.numer_wyswietlacza;
		display.numer_wyswietlacza++;

		seg = znakNa16seg(display.segmenty[4][0],display.segmenty[4][1]);
		break;
		case VII:
		wys = 0x8000>>display.numer_wyswietlacza;
		display.numer_wyswietlacza++;

		seg = znakNa16seg(display.segmenty[5][0],display.segmenty[5][1]);
		break;
		case VIII:
		wys = 0x8000>>display.numer_wyswietlacza;
		display.numer_wyswietlacza++;

		seg = znakNa16seg(display.segmenty[6][0],display.segmenty[6][1]);
		break;
		case IX:
		wys = 0x8000>>display.numer_wyswietlacza;
		display.numer_wyswietlacza++;

		seg = znakNa16seg(display.segmenty[7][0],display.segmenty[7][1]);
		break;
		case X: //rpm
		wys = 0x8000>>display.numer_wyswietlacza;
		display.numer_wyswietlacza++;

		seg = display.rpm;
		break;
		case XI: //flagi
		wys = 0x8000>>display.numer_wyswietlacza;
		display.numer_wyswietlacza = 0;

		seg = flagi_16seg(display.flagi);
		break;
	}
	spi_zapisz(wys,seg);
}

/** Configures the board hardware and chip peripherals for the demo's functionality. */
void SetupHardware(void)
{
#if (ARCH == ARCH_AVR8)
	/* Disable watchdog if enabled by bootloader/fuses */
	MCUSR &= ~(1 << WDRF);
	wdt_disable();

	/* Disable clock division */
	clock_prescale_set(clock_div_1);
#elif (ARCH == ARCH_XMEGA)
	/* Start the PLL to multiply the 2MHz RC oscillator to 32MHz and switch the CPU core to run from it */
	XMEGACLK_StartPLL(CLOCK_SRC_INT_RC2MHZ, 2000000, F_CPU);
	XMEGACLK_SetCPUClockSource(CLOCK_SRC_PLL);

	/* Start the 32MHz internal RC oscillator and start the DFLL to increase it to 48MHz using the USB SOF as a reference */
	XMEGACLK_StartInternalOscillator(CLOCK_SRC_INT_RC32MHZ);
	XMEGACLK_StartDFLL(CLOCK_SRC_INT_RC32MHZ, DFLL_REF_INT_USBSOF, F_USB);

	PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
#endif

	/* Hardware Initialization */
	Joystick_Init();
	USB_Init();
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
	LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	ConfigSuccess &= HID_Device_ConfigureEndpoints(&Mouse_HID_Interface);
	ConfigSuccess &= CDC_Device_ConfigureEndpoints(&VirtualSerial_CDC_Interface);

	USB_Device_EnableSOFEvents();

	LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
	CDC_Device_ProcessControlRequest(&VirtualSerial_CDC_Interface);
	HID_Device_ProcessControlRequest(&Mouse_HID_Interface);
}

/** Event handler for the USB device Start Of Frame event. */
void EVENT_USB_Device_StartOfFrame(void)
{
	HID_Device_MillisecondElapsed(&Mouse_HID_Interface);
}

/** HID class driver callback function for the creation of HID reports to the host.
 *
 *  \param[in]     HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in,out] ReportID    Report ID requested by the host if non-zero, otherwise callback should set to the generated report ID
 *  \param[in]     ReportType  Type of the report to create, either HID_REPORT_ITEM_In or HID_REPORT_ITEM_Feature
 *  \param[out]    ReportData  Pointer to a buffer where the created report should be stored
 *  \param[out]    ReportSize  Number of bytes written in the report (or zero if no report is to be sent)
 *
 *  \return Boolean \c true to force the sending of the report, \c false to let the library determine if it needs to be sent
 */
bool CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize)
{
	USB_MouseReport_Data_t* MouseReport = (USB_MouseReport_Data_t*)ReportData;

	//uint32_t przycisk3_4 = (PRZYCISK_3 << 1 | PRZYCISK_4) << ((switch14Stable-1)*2);
	uint16_t przyciski = PRZYCISK_1 | (PRZYCISK_2 << 1) | (PRZYCISK_3 << 2) | (PRZYCISK_4 << 3) | (PRZYCISK_5 << 4) | (PRZYCISK_6 << 5) | (PRZYCISK_7 << 6) | (PRZYCISK_8 << 7) | (PRZYCISK_9 << 8) | (PRZYCISK_10 << 9) | (PRZYCISK_17 << 10) | (PRZYCISK_18 << 11) | (PRZYCISK_25 << 12) | (PRZYCISK_26 << 13);
	MouseReport->Button = przyciski;
	MouseReport->X = ENCODER_GORA;
	MouseReport->Y = ENCODER_DOL;
	//MouseReport->Z = 0;
	
	ReportData = MouseReport;
	*ReportSize = sizeof(USB_MouseReport_Data_t);
	return true;
}

/** HID class driver callback function for the processing of HID reports from the host.
 *
 *  \param[in] HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in] ReportID    Report ID of the received report from the host
 *  \param[in] ReportType  The type of report that the host has sent, either HID_REPORT_ITEM_Out or HID_REPORT_ITEM_Feature
 *  \param[in] ReportData  Pointer to a buffer where the received report has been stored
 *  \param[in] ReportSize  Size in bytes of the received HID report
 */
void CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void* ReportData,
                                          const uint16_t ReportSize)
{
	// Unused (but mandatory for the HID class driver) in this demo, since there are no Host->Device reports
}

