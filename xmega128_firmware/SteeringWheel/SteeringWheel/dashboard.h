/*
 * dashboard.h
 *
 * Created: 2014-02-27 12:19:46
 *  Author: Pawel
 */ 


#ifndef DASHBOARD_H_
#define DASHBOARD_H_
#include <avr/io.h>
#include "spi.h"
#include "wyswietlacz.h"
#include "tcc0.h"

struct Display {
	uint8_t numer_wyswietlacza;
	uint16_t rpm;
	uint8_t bieg;
	uint8_t segmenty[8][2];
	uint8_t flagi;
};

enum {I=0,II,III,IV,V,VI,VII,VIII,IX,X,XI};

//"Alfabet na 14 segmentowy wyswietlacz: 0123456789AbCcdEFGHhIJKLMNnOPRrSTtUuWXYZ+-/\"

volatile struct Display display;
volatile uint16_t wys = 0;
volatile uint16_t seg = 0;


#define ROZM_BUFORU 30
volatile unsigned char odb[ROZM_BUFORU];
volatile unsigned char wyslij[20];
volatile int liczba = 0;
volatile uint8_t flaga = 0;
volatile uint8_t ile_odebrano = -1;

void dashboard_display_init(void)
{
	display.numer_wyswietlacza = 0;
	////////////////===============X
	display.rpm = 0b1111111111111111;
	display.bieg = '4';
	display.segmenty[0][0] = '0';
	display.segmenty[0][1] = '.';
	display.segmenty[1][0] = '1';
	display.segmenty[1][1] = '.';
	display.segmenty[2][0] = '2';
	display.segmenty[2][1] = '.';
	display.segmenty[3][0] = '3';
	display.segmenty[3][1] = '.';
	display.segmenty[4][0] = '5';
	display.segmenty[4][1] = '.';
	display.segmenty[5][0] = '6';
	display.segmenty[5][1] = '.';
	display.segmenty[6][0] = '7';
	display.segmenty[6][1] = '.';
	display.segmenty[7][0] = '8';
	display.segmenty[7][1] = '.';
	//////////////////========
	display.flagi = 0b11111111;
}

void dashboard_init(void)
{
	PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm; //W��czamy wszystkie poziomy przerwa� (potrzebujemy tylko najni�szy)
	sei();
	tcc0_init();
	spi_init();
	dashboard_display_init();
}

#endif /* DASHBOARD_H_ */