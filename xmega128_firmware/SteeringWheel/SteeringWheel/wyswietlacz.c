/*
 * wyswietlacz.c
 *
 *  Created on: 28-09-2013
 *      Author: Pawel
 */
#include "wyswietlacz.h"
#include <stdio.h>

#define F_NIEBIESKA_L  1
#define F_NIEBIESKA_P  2

#define F_CZERWONA_L   3
#define F_CZERWONA_P   4

#define F_ZOLTA_L      5
#define F_ZOLTA_P      6

#define KERS           7
#define DRS            8


#define SET(x,y) (x|=(1<<y))
#define CLR(x,y) (x&=(~(1<<y)))
#define CHK(x,y) (x&(1<<y))
#define TOG(x,y) (x^=(1<<y))


/* 14 Seg
		a
f	g	h	j	b
	p	    k		
e	n	m	l	c
		d
*/

// 7 Seg
//		a
//f				b
//		g
//e				c
//		d          D

uint16_t znakNa7seg(signed char znak)
{
	uint16_t diody = 0xff;
	switch(znak)
	{
	case '0':
	    //========abcdefxxxggxxxDx
		diody = 0b1111110000000000; //0
		break;
	case '1':
	    //========abcdefxxxggxxxDx
		diody = 0b0110000000000000; //1
		break;
	case '2':
	    //========abcdefxxxggxxxDx
		diody = 0b1101100001100000; //2
		break;
	case '3':
	    //========abcdefxxxggxxxDx
		diody = 0b1111000001100000; //3
		break;
	case '4':
	    //========abcdefxxxggxxxDx
		diody = 0b0110010001100000; //4
		break;
	case '5':
	    //========abcdefxxxgxxxxDx
		diody = 0b1011010001000000; //5
		break;
	case '6':
	    //========abcdefxxxggxxxDx
		diody = 0b1011110001100000; //6
		break;
	case '7':
	    //========abcdefxxxggxxxDx
		diody = 0b1110000000000000; //7
		break;
	case '8':
	    //========abcdefxxxggxxxDx
		diody = 0b1111110001100000; //8
		break;
	case '9':
	    //========abcdefxxxggxxxDx
		diody = 0b1111010001100000; //9
		break;
	case 'A':
	    //========abcdefxxxggxxxDx
		diody = 0b1110110001100010; //A
		break;
	case 'C':
	    //========abcdefxxxggxxxDx
		diody = 0b1001110000000010; //C
		break;
	case 'E':
	    //========abcdefxxxggxxxDx
		diody = 0b1001110001100010; //E
		break;
	case 'F':
	    //========abcdefxxxggxxxDx
		diody = 0b1000110001100010; //F
		break;
	case 'H':
	    //========abcdefxxxggxxxDx
		diody = 0b0110110001100010; //H
		break;
	case 'J':
	    //========abcdefxxxggxxxDx
		diody = 0b1111000000000010; //J
		break;
	case 'L':
	    //========abcdefxxxggxxxDx
		diody = 0b0001110000000010; //L
		break;
	case 'O':
	    //========abcdefxxxggxxxDx
		diody = 0b1111110000000010; //O
		break;
	case 'P':
	    //========abcdefxxxggxxxDx
		diody = 0b1100110001100010; //P
		break;
	case 'S':
	    //========abcdefxxxggxxxDx
		diody = 0b1011010001100010; //S
		break;
	case 'U':
	    //========abcdefxxxggxxxDx
		diody = 0b0111110000000010; //U
		break;
	case '-':
	    //========abcdefxxxggxxxDx
		diody = 0b0000000001100010; //-
		break;
	case 'n':
	    //========abcdefxxxggxxxDx
		diody = 0b0010100001100010; //n
		break;
	case 'r':
	    //========abcdefxxxggxxxDx
		diody = 0b0000100001100010; //r
		break;
	case ' ':
	    //========abcdefxxxggxxxDx
		diody = 0b0000000000000000; //pusty
		break;
	default:
		diody = 0;
		/* no break */
	}

	return diody;
}

uint16_t znakNa16seg(signed char znak,uint8_t kropka)
{
	uint16_t diody = 0xff;
	switch(znak)
	{
	case '0':
	    //========abcdefghjpknmlDx
		diody = 0b1111110000000000; //0
		break;
	case '1':
	    //========abcdefghjpknmlDx
		diody = 0b0110000000000000; //1
		break;
	case '2':
	    //========abcdefghjpknmlDx
		diody = 0b1101100001100000; //2
		break;
	case '3':
	    //========abcdefghjpknmlDx
		diody = 0b1111000000100000; //3
		break;
	case '4':
	    //========abcdefghjpknmlDx
		diody = 0b0110010001100000; //4
		break;
	case '5':
	    //========abcdefghjpknmlDx
		diody = 0b1011010001100000; //5
		break;
	case '6':
	    //========abcdefghjpknmlDx
		diody = 0b1011110001100000; //6
		break;
	case '7':
	    //========abcdefghjpknmlDx
		diody = 0b1110000000000000; //7
		break;
	case '8':
	    //========abcdefghjpknmlDx
		diody = 0b1111110001100000; //8
		break;
	case '9':
	    //========abcdefghjpknmlDx
		diody = 0b1110010001100000; //9
		break;
	case 'A':
	    //========abcdefghjpknmlDx
		diody = 0b1110110001100000; //A
		break;
	case 'C':
	    //========abcdefghjpknmlDx
		diody = 0b1001110000000000; //C
		break;
	case 'E':
	    //========abcdefghjpknmlDx
		diody = 0b1001110001100000; //E
		break;
	case 'F':
	    //========abcdefghjpknmlDx
		diody = 0b1000110001000000; //F
		break;
	case 'G':
	    //========abcdefghjpknmlDx
		diody = 0b1011110000100000; //G
		break;
	case 'H':
	    //========abcdefghjpknmlDx
		diody = 0b0110110001100000; //H
		break;
	case 'I':
	    //========abcdefghjpknmlDx
		diody = 0b0000000100001000; //I
		break;
	case 'J':
	    //========abcdefghjpknmlDx
		diody = 0b0111000000000000; //J
		break;
	case 'K':
	    //========abcdefghjpknmlDx
		diody = 0b0000110011000100; //K
		break;
	case 'L':
	    //========abcdefghjpknmlDx
		diody = 0b0001110000000000; //L
		break;
	case 'M':
	    //========abcdefghjpknmlDx
		diody = 0b0110111010000000; //M
		break;
	case 'N':
	    //========abcdefghjpknmlDx
		diody = 0b0110111000000100; //N
		break;
	case 'O':
	    //========abcdefghjpknmlDx
		diody = 0b1111110000000000; //O
		break;
	case 'P':
	    //========abcdefghjpknmlDx
		diody = 0b1100110001100000; //P
		break;
	case 'R':
	    //========abcdefghjpknmlDx
		diody = 0b1100110001100100; //R
		break;
	case 'S':
	    //========abcdefghjpknmlDx
		diody = 0b1011010001100000; //S
		break;
	case 'T':
	    //========abcdefghjpknmlDx
		diody = 0b1000000100001000; //T
		break;
	case 'U':
	    //========abcdefghjpknmlDx
		diody = 0b0111110000000000; //U
		break;
	case 'W':
	    //========abcdefghjpknmlDx
		diody = 0b0110110000010100; //W
		break;
	case 'X':
	    //========abcdefghjpknmlDx
		diody = 0b0000001010010100; //X
		break;
	case 'Y':
	    //========abcdefghjpknmlDx
		diody = 0b0000001010001000; //Y
		break;
	case 'Z':
	    //========abcdefghjpknmlDx
		diody = 0b1001000010010000; //Z
		break;
	case 'b':
	    //========abcdefghjpknmlDx
		diody = 0b0011110001100000; //b
		break;
	case 'c':
	    //========abcdefghjpknmlDx
		diody = 0b0001100001100000; //c
		break;
	case 'd':
	    //========abcdefghjpknmlDx
		diody = 0b0111100001100000; //d
		break;
	case 'h':
	    //========abcdefghjpknmlDx
		diody = 0b0010110001100000; //h
		break;
	case 'n':
	    //========abcdefghjpknmlDx
		diody = 0b0010100001100000; //n
		break;
	case 'r':
	    //========abcdefghjpknmlDx
		diody = 0b0000100001100000; //r
		break;
	case 't':
	    //========abcdefghjpknmlDx
		diody = 0b0001110001000000; //t
		break;
	case 'u':
	    //========abcdefghjpknmlDx
		diody = 0b0011100000000000; //u
		break;
	case '+':
	    //========abcdefghjpknmlDx
		diody = 0b0000000101101000; //+
		break;
	case '-':
	    //========abcdefghjpknmlDx
		diody = 0b0000000001100000; //-
		break;
	case '/':
	    //========abcdefghjpknmlDx
		diody = 0b0000000010010000; // /
		break;
	case '\\':
	    //========abcdefghjpknmlDx
		diody = 0b0000001000000100; // '\\'
		break;
	case ' ':
	    //========abcdefxxxggxxxDx
		diody = 0b0000000000000000; //pusty
		break;
	default:
		diody = 0;
		/* no break */
	}
	if(diody != 0)
	{
		if(kropka == 0x2E)
		{
			diody |= 0b0000000000000010;
		}
	}

	return diody;
}

uint16_t biegNa7seg(signed char bieg)
{
	uint16_t diody = 0xff;
	switch(bieg)
	{
	case 1:
	    //========abcdefxxxggxxxDx
		diody = 0b0110000000000000; //1
		break;
	case 2:
	    //========abcdefxxxggxxxDx
		diody = 0b1101100001100000; //2
		break;
	case 3:
	    //========abcdefxxxggxxxDx
		diody = 0b1111000001100000; //3
		break;
	case 4:
	    //========abcdefxxxggxxxDx
		diody = 0b0110010001100000; //4
		break;
	case 5:
	    //========abcdefxxxggxxxDx
		diody = 0b1011010001100000; //5
		break;
	case 6:
	    //========abcdefxxxggxxxDx
		diody = 0b1011110001100000; //6
		break;
	case 7:
	    //========abcdefxxxggxxxDx
		diody = 0b1110000000000000; //7
		break;
	case 8:
	    //========abcdefxxxggxxxDx
		diody = 0b1111110001100000; //8
		break;
	case 9:
	    //========abcdefxxxggxxxDx
		diody = 0b1111010001100000; //9
		break;
	case 0:
	    //========abcdefxxxggxxxDx
		diody = 0b0010100001100010; //n
		break;
	case -1:
	    //========abcdefxxxggxxxDx
		diody = 0b0000100001100010; //r
		break;
	default:
	    //========abcdefxxxggxxxDx
		diody = 0b0000000001100010; //-
		/* no break */
	}
	return diody;
}

uint16_t flagi_16seg(uint8_t flagi)
{
	uint16_t diody = 0;

	if(CHK(flagi,(8-F_NIEBIESKA_L)) != 0)
	{
		diody |= (1<<(16-F_NIEBIESKA_L));
	}
	if(CHK(flagi,(8-F_NIEBIESKA_P)) != 0)
	{
		diody |= (1<<(16-F_NIEBIESKA_P));
	}
	if(CHK(flagi,(8-F_CZERWONA_L)) != 0)
	{
		diody |= (1<<(16-F_CZERWONA_L));
	}
	if(CHK(flagi,(8-F_CZERWONA_P)) != 0)
	{
		diody |= (1<<(16-F_CZERWONA_P));
	}
	if(CHK(flagi,(8-F_ZOLTA_L)) != 0)
	{
		diody |= (1<<(16-F_ZOLTA_L));
	}
	if(CHK(flagi,(8-F_ZOLTA_P)) != 0)
	{
		diody |= (1<<(16-F_ZOLTA_P));
	}
	if(CHK(flagi,(8-DRS)) != 0)
	{
		diody |= (1<<(16-DRS));
	}
	if(CHK(flagi,(8-KERS)) != 0)
	{
		diody |= (1<<(16-KERS));
	}
	return diody;
}
