/*
 * tcc0.h
 *
 * Created: 2014-02-27 12:26:43
 *  Author: Pawel
 */ 


#ifndef TCC0_H_
#define TCC0_H_
#include <avr/io.h>

void tcc0_init(void)
{
	TCC0.CTRLA = TC_CLKSEL_DIV1024_gc; //prescaler 1024
	TCC0.INTCTRLA = TC_OVFINTLVL_HI_gc; //priorytet HIGH
	TCC0.PER = 20; //aby przerwanie bylo z f =  Hz gdy F_CPU=32MHz
}


#endif /* TCC0_H_ */