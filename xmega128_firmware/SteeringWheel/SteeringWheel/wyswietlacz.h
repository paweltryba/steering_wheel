/*
 * wyswietlacz.h
 *
 *  Created on: 28-09-2013
 *      Author: Pawel
 */

#ifndef WYSWIETLACZ_H_
#define WYSWIETLACZ_H_
#include <avr/io.h>
#include <stdio.h>

uint16_t znakNa7seg(signed char znak);
uint16_t znakNa16seg(signed char znak,uint8_t kropka);
uint16_t biegNa7seg(signed char bieg);
uint16_t flagi_16seg(uint8_t flagi);



#endif /* WYSWIETLACZ_H_ */
