/*
 * pwm.h
 *
 * Created: 2014-02-28 12:11:54
 *  Author: Pawel
 */ 


#ifndef PWM_H_
#define PWM_H_
#include <avr/io.h>

void pwm_duty_set(int16_t ile)
{
	if(ile < 0) //LEWO
	{
		PORTE.OUTCLR = PIN1_bm;
		PORTE.OUTSET = PIN2_bm;
		TCE0.CCA = -ile+1000;
	}
	else if(ile > 0) //PRAWO
	{
		PORTE.OUTSET = PIN1_bm;
		PORTE.OUTCLR = PIN2_bm;
		TCE0.CCA = ile+1000;
	}
	else //STOP
	{
		PORTE.OUTCLR = PIN1_bm;
		PORTE.OUTCLR = PIN2_bm;
		TCE0.CCA = 0;
	}
	
}

void pwm_init(void)
{
	PORTE.DIRSET = PIN0_bm; //PWM OUTPUT  OC0A
	PORTE.OUTCLR = PIN0_bm; //PWM CLEAR  OC0A
	
	PORTE.DIRSET = PIN1_bm | PIN2_bm; //OUT
	PORTE.OUTCLR = PIN1_bm | PIN2_bm; //OUT
	
	TCE0.PER = 2400; 
	TCE0.CTRLA |= TC_CLKSEL_DIV8_gc;
	TCE0.CTRLB |= TC0_CCAEN_bm;
	TCE0.CTRLB |= TC_WGMODE_SINGLESLOPE_gc;
	
	TCE0.CCA = 0;
}






#endif /* PWM_H_ */